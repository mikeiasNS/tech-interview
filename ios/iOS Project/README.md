
# Joyjet Tech Interview

iOS project following the specs from this repository

## Guidelines

- Download [Cocoapods](https://cocoapods.org/app);
- Run `pod install` in a terminal window from this folder;
- Open the project in Xcode using the `.xcworkspace` file;
- Run it on an iOS device or simulator;
