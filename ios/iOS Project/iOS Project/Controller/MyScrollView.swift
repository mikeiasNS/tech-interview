//
//  MyScrollView.swift
//  iOS Project
//
//  Created by Mikeias Nascimento on 22/06/2018.
//  Copyright © 2018 Mikeias Nascimento. All rights reserved.
//

import UIKit

// Subclass of ScrollView created to make sure that UITableView will be selected when tapped even with a ScrollView inside
class MyScrollView: UIScrollView {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        superview?.touchesBegan(touches, with: event)
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        superview?.touchesEnded(touches, with: event)
    }
}
