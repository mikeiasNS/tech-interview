//
//  DetailsVC.swift
//  iOS Project
//
//  Created by Mikeias Nascimento on 21/06/2018.
//  Copyright © 2018 Mikeias Nascimento. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class DetailsVC: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var descriptionText: UITextView!
    @IBOutlet weak var scroller: UIScrollView!
    
    let managedContext = DataManager().persistentContainer.viewContext
    var favorite: NSManagedObject? = nil
    var item = NSDictionary()
    var isListingFavorites = false
    
    override func viewDidLoad() {
        makeNavigationBarTransparent()
        favorite = favoriteItemManagedObject()
        
        setBarButtons()
        setInfo()
    }
    
    func makeNavigationBarTransparent() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
    }
    
    func setInfo() {
        if let itemTitle = item[JsonKeys.titleKey] as? String {
            titleLabel.text = itemTitle
        }
        if let itemDescription = item[JsonKeys.descriptionKey] as? String {
            descriptionText.text = itemDescription
        }
        
        if let itemCategory = item[JsonKeys.categoryKey] as? String {
            categoryLabel.text = itemCategory
        }
        
        if let urls = item[JsonKeys.galleryKey] as? [String] {
            Utils.putImagesInScrollView(scrollView: scroller, urls: urls,
                                        width: view.frame.width)
        }
    }
    
    func setBarButtons() {
        let backButton = UIBarButtonItem(image: UIImage(named: "ic_back"),
                                         style: UIBarButtonItemStyle.plain,
                                         target: self,
                                         action: #selector(pop))
        
        let favoritateItemButton = UIBarButtonItem(image: UIImage(named: "ic_star"),
                                                   style: .plain,
                                                   target: self,
                                                   action: #selector(handleStarClick))
        
        backButton.tintColor = Colors.WHITE
        if favorite != nil {
            favoritateItemButton.tintColor = Colors.BLUE
        } else {
            favoritateItemButton.tintColor = Colors.WHITE
        }
        
        navigationItem.setLeftBarButton(backButton, animated: false)
        navigationItem.setRightBarButton(favoritateItemButton, animated: false)
    }
    
    func favoriteItemManagedObject() -> NSManagedObject? {
        var result: NSManagedObject? = nil
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: Entities.favoriteItem)
        request.predicate = NSPredicate(format: "\(Entities.Attributes.category) == '\(item[JsonKeys.categoryKey]!)' and \(Entities.Attributes.title) == '\(item[JsonKeys.titleKey]!)'")
        
        do {
            let results = try managedContext.fetch(request)
            result = results.first as? NSManagedObject
        } catch {
            fatalError("Error retriving favorite item")
        }
        
        return result
    }
    
    @objc func handleStarClick() {
        if favorite != nil {
            disfavorite()
        } else {
            favoritate()
        }
        
        updateListVCIfListingFavorites()
    }
    
    func updateListVCIfListingFavorites() {
        if isListingFavorites {
            NotificationCenter.default.post(Notification(name: Notification.Name(NotificationKeys.favoritesSelected)))
        }
    }
    
    func disfavorite() {
        do {
            managedContext.delete(favorite!)
            try managedContext.save()
            
            navigationItem.rightBarButtonItem?.tintColor = Colors.WHITE
        } catch {
            fatalError("Error removing favorite item")
        }
    }
    
    func favoritate() {
        let entity = NSEntityDescription.entity(forEntityName: Entities.favoriteItem, in: managedContext)!
        let favorite = NSManagedObject(entity: entity, insertInto: managedContext)
        
        favorite.setValue(item[JsonKeys.titleKey] as! String, forKey: Entities.Attributes.title)
        favorite.setValue(item[JsonKeys.descriptionKey] as! String, forKey: Entities.Attributes.description)
        favorite.setValue(item[JsonKeys.categoryKey] as! String, forKey: Entities.Attributes.category)
        favorite.setValue(item[JsonKeys.galleryKey] as! [String], forKey: Entities.Attributes.gallery)
        
        navigationItem.rightBarButtonItem?.tintColor = Colors.BLUE
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    @objc func pop() {
        navigationController?.popViewController(animated: true)
    }
}
