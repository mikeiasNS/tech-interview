//
//  ListVC.swift
//  iOS Project
//
//  Created by Mikeias Nascimento on 21/06/2018.
//  Copyright © 2018 Mikeias Nascimento. All rights reserved.
//

import UIKit
import Alamofire
import CoreData

class ListVC: UIViewController, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    
    let jsonUrl = "https://cdn.joyjet.com/tech-interview/mobile-test-one.json"
    var data = [NSDictionary]()
    let tableViewInternetDatasource = ItemsFromWebDatasource(data: [NSDictionary]())
    let tableViewFavoritesDatasource = FavoriteItemsDataSource(data: [NSDictionary]())
    var listingFavorites = false
    
    let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        setWebDataSource()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(setFavoritesDataSource),
                                               name: NSNotification.Name(NotificationKeys.favoritesSelected),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(setWebDataSource),
                                               name: NSNotification.Name(NotificationKeys.homeSelected),
                                               object: nil)
        
        let textAttributes = [NSAttributedStringKey.foregroundColor: Colors.BLUE]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
    }
    
    @objc func refresh() {
        if listingFavorites {
            loadFavorites()
        } else {
            loadFromWeb()
        }
        refreshControl.endRefreshing()
    }
    
    @objc func setWebDataSource() {
        tableView.dataSource = tableViewInternetDatasource
        tableView.sectionHeaderHeight = 33
        loadFromWeb()
        
        navigationItem.title = "Digital Space"
    }
    
    @objc func setFavoritesDataSource() {
        tableView.dataSource = tableViewFavoritesDatasource
        tableView.sectionHeaderHeight = 0
        loadFavorites()
        
        navigationItem.title = "Favorites"
    }
    
    func loadFromWeb() {
        Alamofire.request(jsonUrl).validate(statusCode: 200..<300).responseJSON { response in
            if let result = response.result.value, let json = result as? [NSDictionary] {
                self.tableViewInternetDatasource.data = json
                self.data = json
                self.tableView.reloadData()
                self.listingFavorites = false
            }
        }
    }
    
    func loadFavorites() {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Entities.favoriteItem)
        let context = DataManager().persistentContainer.viewContext
        
        do {
            let results = try context.fetch(fetchRequest)
            let data = results as! [NSManagedObject]
            var dictData = [NSMutableDictionary]()
            
            for item in data {
                let dict = NSMutableDictionary()
                
                dict[JsonKeys.titleKey] = item.value(forKey: Entities.Attributes.title) as? String
                dict[JsonKeys.descriptionKey] = item.value(forKey: Entities.Attributes.description) as? String
                dict[JsonKeys.categoryKey] = item.value(forKey: Entities.Attributes.category) as? String
                dict[JsonKeys.galleryKey] = item.value(forKey: Entities.Attributes.gallery) as? [String]
                
                dictData.append(dict)
            }
            
            tableViewFavoritesDatasource.data = dictData
            self.data = dictData
            tableView.reloadData()
            listingFavorites = true
        } catch {
            fatalError("Error retriving favorite items")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailsSegue" {
            if let nextVC = segue.destination as? DetailsVC, let item = sender as? NSDictionary {
                nextVC.item = item
                nextVC.isListingFavorites = listingFavorites
            }
        }
    }
    
    //    MARK: tableView Delegate methods
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 209
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 33))
        let headerLabel = UILabel(frame: CGRect(x: 16, y: 0, width: headerView.bounds.width, height: headerView.bounds.height))
        headerLabel.text = data[section][JsonKeys.categoryKey] as? String
        
        if let font = UIFont(name: "Montserrat-SemiBold", size: UIFont.labelFontSize) {
            headerLabel.font = font
        }
        
        headerView.backgroundColor = Colors.BLUE
        headerLabel.textColor = Colors.WHITE
        headerView.addSubview(headerLabel)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let item = (data[indexPath.section][JsonKeys.itemsKey] as? [NSDictionary])?[indexPath.row] {
            let mutableItem = item.mutableCopy() as! NSMutableDictionary
            
            mutableItem[JsonKeys.categoryKey] = data[indexPath.section][JsonKeys.categoryKey] as! String
            performSegue(withIdentifier: "detailsSegue", sender: mutableItem)
        } else {
            let item = data[indexPath.row]
            performSegue(withIdentifier: "detailsSegue", sender: item)
        }
    }
}
