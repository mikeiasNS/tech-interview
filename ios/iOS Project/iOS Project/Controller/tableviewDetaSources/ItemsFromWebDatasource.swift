//
//  ItemsFromWebDatasource.swift
//  iOS Project
//
//  Created by Mikeias Nascimento on 21/06/2018.
//  Copyright © 2018 Mikeias Nascimento. All rights reserved.
//

import Foundation
import UIKit

class ItemsFromWebDatasource: NSObject, UITableViewDataSource {
    var data = [NSDictionary]()
    
    init(data: [NSDictionary]) {
        self.data = data
        super.init()
    }
    
    convenience override init() {
        self.init(data: [NSDictionary]())
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var result = 0
        if let itemsCount = (data[section][JsonKeys.itemsKey] as? [NSDictionary])?.count {
            result = itemsCount
        }
        
        return result
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! GalleryCell
        
        if let item = (data[indexPath.section][JsonKeys.itemsKey] as? [NSDictionary])?[indexPath.row] {
            if let title = item[JsonKeys.titleKey] as? String {
                cell.titleLabel.text = title
            }
            if let description = item[JsonKeys.descriptionKey] as? String {
                let index = description.index(description.startIndex, offsetBy: 50)
                cell.descriptionLabel.text = String(description[..<index])
            }
            if let urls = item[JsonKeys.galleryKey] as? [String] {
                cell.setGalleryUrls(galleryUrls: urls)
            }
        }
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return data.count
    }
    
}
