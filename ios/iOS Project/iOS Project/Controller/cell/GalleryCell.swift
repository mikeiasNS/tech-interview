//
//  GalleryCell.swift
//  iOS Project
//
//  Created by Mikeias Nascimento on 21/06/2018.
//  Copyright © 2018 Mikeias Nascimento. All rights reserved.
//

import UIKit
import SDWebImage

class GalleryCell: UITableViewCell {
    private var galleryUrls = [String]()
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var galleryScroller: UIScrollView!
    
    func setGalleryUrls(galleryUrls: [String]) {
        self.galleryUrls = galleryUrls
        Utils.putImagesInScrollView(scrollView: galleryScroller, urls: galleryUrls,
                                    width: frame.width)
    }
}
