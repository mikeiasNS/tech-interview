//
//  MenuVC.swift
//  iOS Project
//
//  Created by Mikeias Nascimento on 21/06/2018.
//  Copyright © 2018 Mikeias Nascimento. All rights reserved.
//

import Foundation
import UIKit
import SideMenu

class MenuVC: UIViewController {
    fileprivate func dismissMenu() {
        if let menu = navigationController as? UISideMenuNavigationController {
            menu.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func homeClicked(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(NotificationKeys.homeSelected), object: nil)
        dismissMenu()
    }
    @IBAction func favoritesClicked(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(NotificationKeys.favoritesSelected), object: nil)
        dismissMenu()
    }
    
    override func viewDidLoad() {
    }
}
