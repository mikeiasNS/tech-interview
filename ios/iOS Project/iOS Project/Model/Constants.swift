//
//  Constants.swift
//  iOS Project
//
//  Created by Mikeias Nascimento on 21/06/2018.
//  Copyright © 2018 Mikeias Nascimento. All rights reserved.
//

import Foundation
import  UIKit

struct Colors {
    static let BLUE = UIColor(red: 0.29, green: 0.56, blue: 0.88, alpha: 1)
    static let WHITE = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
}

struct JsonKeys {
    static let itemsKey = "items"
    static let categoryKey = "category"
    static let titleKey = "title"
    static let descriptionKey = "description"
    static let galleryKey = "galery"
}

struct NotificationKeys {
    static let homeSelected = "homeSelected"
    static let favoritesSelected = "favoritesSelected"
    static let updateFavorites = "updateFavorites"
}

struct Entities {
    static let favoriteItem = "FavoriteItem"
    struct Attributes {
        static let title = "itemTitle"
        static let description = "itemDescription"
        static let category = "itemCategory"
        static let gallery = "galleryUrls"
    }
}

class Utils {
    static func putImagesInScrollView(scrollView: UIScrollView, urls: [String], width: CGFloat) {
        let scrollerWidth = CGFloat(urls.count) * width
        let height = scrollView.frame.height
        
        scrollView.contentSize = CGSize(width: scrollerWidth, height: height)
        scrollView.isPagingEnabled = true
        
        for i in 0..<urls.count {
            let url = URL(string: urls[i])
            let imageView = UIImageView(frame: CGRect(x: CGFloat(i) * width, y: 0,
                                                      width: width, height: height))
            
            scrollView.addSubview(imageView)
            
            imageView.sd_setImage(with: url) { (image, error, cacheType, url) in
                if error != nil {
                    imageView.image = UIImage(named: "ic_image")
                }
            }
        }
    }
}
