package com.mikeias.joyjetandroid.controller.listItems

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.mikeias.joyjetandroid.R
import com.mikeias.joyjetandroid.controller.SimpleRecyclerView
import com.mikeias.joyjetandroid.controller.GalleryPagerAdapter
import com.mikeias.joyjetandroid.controller.activities.DetailsActivity
import com.squareup.picasso.Callback
import kotlinx.android.synthetic.main.item_category.view.*
import kotlinx.android.synthetic.main.item_gallery.view.*
import java.io.Serializable

class GalleryCategoryItem(val title: String?, val description: String?, val gallery: ArrayList<String>?,
                          val category: String): SimpleRecyclerView.Item(), Serializable {
    companion object {
        const val GALLERY_VIEW_TYPE = 0
        const val CATEGORY_VIEW_TYPE = 1
    }
    constructor(category: String): this(null, null, null, category)

    override fun onCreateView(parent: ViewGroup, inflater: LayoutInflater, viewType: Int): View {
        return if(viewType == CATEGORY_VIEW_TYPE) {
            inflater.inflate(R.layout.item_category, parent, false)
        } else {
            inflater.inflate(R.layout.item_gallery, parent, false)
        }
    }

    fun onClickItem(context: Context?) {
        if (getViewType() == GALLERY_VIEW_TYPE) {
            val intent = Intent(context, DetailsActivity::class.java)
            intent.putExtra(DetailsActivity.ITEM_EXTRA, this)
            context?.startActivity(intent)
        }
    }

    override fun bindViews(itemView: View) {
        if(getViewType() == GALLERY_VIEW_TYPE) {
            itemView.title.text = title

            val shortDescriptionMaxCharacters = 50
            val shortDescription = if(shortDescriptionMaxCharacters < description!!.length) {
                itemView.resources.getString(R.string.ellipsis_text,
                        description.substring(0 until shortDescriptionMaxCharacters))
            } else {
                description
            }
            itemView.short_description.text = shortDescription
            itemView.gallery_view_pager.adapter = GalleryPagerAdapter(gallery!!)

            itemView.bringToFront()
        } else {
            itemView.category.text = category
        }
    }

    override fun getViewType(): Int {
        var result = GALLERY_VIEW_TYPE
        if(title == null) {
            result = CATEGORY_VIEW_TYPE
        }

        return result
    }

    class ImageLoadedCallback(private val loader: View?, private val imageView: ImageView?): Callback {
        override fun onSuccess() {
            loader?.visibility = View.GONE
            imageView?.scaleType = ImageView.ScaleType.CENTER_CROP
        }

        override fun onError(exeption: Exception?) {
            loader?.visibility = View.GONE
            imageView?.scaleType = ImageView.ScaleType.CENTER
        }
    }
}