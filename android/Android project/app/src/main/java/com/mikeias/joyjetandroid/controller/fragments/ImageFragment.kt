package com.mikeias.joyjetandroid.controller.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mikeias.joyjetandroid.R
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_image.view.*
import java.lang.Exception

class ImageFragment: Fragment() {
    companion object {
        const val IMAGE_URL = "url"

        fun getInstance(url: String): ImageFragment {
            val fragment = ImageFragment()
            val arguments = Bundle()
            arguments.putString(IMAGE_URL, url)
            fragment.arguments = arguments

            return fragment
        }
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_image, container, false)
    }

    override fun onResume() {
        super.onResume()
        val url = arguments?.getString(IMAGE_URL)

        if(url != null) {
            Picasso.get()
                    .load(url)
                    .centerCrop()
                    .error(R.drawable.ic_image)
                    .into(view?.image_view, ImageLoadedCallback(view?.loader))
        }
    }

    class ImageLoadedCallback(val loader: View? = null): Callback {
        override fun onSuccess() {
            loader?.visibility = View.GONE
        }

        override fun onError(e: Exception?) {
            loader?.visibility = View.GONE
        }
    }
}