package com.mikeias.joyjetandroid.controller.fragments

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mikeias.joyjetandroid.R
import com.mikeias.joyjetandroid.controller.SimpleRecyclerView
import com.mikeias.joyjetandroid.controller.listItems.GalleryCategoryItem
import com.mikeias.joyjetandroid.model.service.InterviewTechService
import kotlinx.android.synthetic.main.fragment_list.*
import okhttp3.ResponseBody
import org.json.JSONArray
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class HomeFragment: Fragment() {
    private val baseURL = "https://cdn.joyjet.com/tech-interview/"
    private val categoryJsonKey = "category"
    private val itemsJsonKey = "items"
    private val titleJsonKey = "title"
    private val descriptionJsonKey = "description"
    private val galleryJsonKey = "galery"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onResume() {
        super.onResume()
        activity?.title = getString(R.string.digital_space)

        requestData()
        recyclerView.setOnItemClick {item: SimpleRecyclerView.Item, _: View ->
            if(item is GalleryCategoryItem) {
                item.onClickItem(context)
            }
        }

        swipe_to_refresh.setOnRefreshListener {
            requestData()
        }
    }

    private fun requestData() {
        recyclerView.setData(mutableListOf())

        swipe_to_refresh.isRefreshing = true
        val service = Retrofit.Builder()
                .baseUrl(baseURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(InterviewTechService::class.java)

        no_data_text.visibility = View.GONE

        service.itens().enqueue(ItemsCallback())
    }

    inner class ItemsCallback: Callback<ResponseBody> {
        override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
            if(view != null) {
                Snackbar.make(view!!, getString(R.string.connection_error), Snackbar.LENGTH_SHORT)
                        .show()

                swipe_to_refresh.isRefreshing = false
            }
        }

        override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
            val ok = 200
            if(response?.code() == ok) {
                setListData(response)
            } else {
                Snackbar.make(view!!, getString(R.string.connection_error), Snackbar.LENGTH_SHORT)
                        .show()
            }

            swipe_to_refresh.isRefreshing = false
        }
    }

    private fun setListData(response: Response<ResponseBody>) {
        val json = JSONArray(response.body()?.string())
        val listItems = arrayListOf<GalleryCategoryItem>()

        try {
            for (i in 0 until json.length()) {
                val jsonObject = json.getJSONObject(i)
                val category = jsonObject.getString(categoryJsonKey)
                listItems.add(GalleryCategoryItem(category))

                val items = jsonObject.getJSONArray(itemsJsonKey)
                for (j in 0 until items.length()) {
                    val itemJson = items.getJSONObject(j)
                    val title = itemJson.getString(titleJsonKey)
                    val description = itemJson.getString(descriptionJsonKey)
                    val galleryJson = itemJson.getJSONArray(galleryJsonKey)
                    val gallery = arrayListOf<String>()
                    for (k in 0 until galleryJson.length()) {
                        gallery.add(galleryJson.getString(k))
                    }

                    listItems.add(GalleryCategoryItem(title, description, gallery, category))
                }
            }

            if(json.length() == 0) {
                no_data_text.visibility = View.VISIBLE
            }
        } catch (exception: Exception) {
            Snackbar.make(view!!, getString(R.string.unknown_error), Snackbar.LENGTH_SHORT)
                    .show()
            exception.printStackTrace()
        }

        recyclerView.setData((listItems as List<SimpleRecyclerView.Item>).toMutableList())
    }
}