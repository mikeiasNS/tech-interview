package com.mikeias.joyjetandroid.model.database

import android.arch.persistence.room.TypeConverter
import com.google.gson.Gson
import org.json.JSONArray

class Converters {
    @TypeConverter
    fun fromArrayListString(arrayString: String): ArrayList<String> {
        val result = arrayListOf<String>()
        val json = JSONArray(arrayString)
        for(i in 0 until json.length()) {
            result.add(json.getString(i))
        }

        return result
    }

    @TypeConverter
    fun ArrayListToString(list: ArrayList<String>): String {
        return Gson().toJson(list)
    }
}