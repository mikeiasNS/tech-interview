package com.mikeias.joyjetandroid.model.service

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET

interface InterviewTechService {
    @GET("mobile-test-one.json")
    fun itens(): Call<ResponseBody>
}