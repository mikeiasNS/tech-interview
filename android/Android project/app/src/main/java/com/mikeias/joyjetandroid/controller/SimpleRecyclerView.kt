package com.mikeias.joyjetandroid.controller

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mikeias.joyjetandroid.R

class SimpleRecyclerView(context: Context, attributes: AttributeSet? = null):
        RecyclerView(context, attributes) {
    private var simpleDivider = false
    private var dividerResource = 0
    private var dividerOrientation = 0

    init {
        adapter = GenericAdapter(mutableListOf(), context)
        handleCustomAttrs(attributes)
    }

    private fun handleCustomAttrs(attributes: AttributeSet?) {
        val attrs = context.theme.obtainStyledAttributes(attributes,
                R.styleable.SimpleRecyclerView, 0, 0)
        try {
            simpleDivider = attrs.getBoolean(R.styleable.SimpleRecyclerView_simpleDivider, false)
            dividerResource = attrs.getResourceId(R.styleable.SimpleRecyclerView_itemDivider, 0)
            dividerOrientation = attrs.getInt(R.styleable.SimpleRecyclerView_dividerOrientation, VERTICAL)
        } finally {
            attrs.recycle()

            if(simpleDivider) {
                val divider = DividerItemDecoration(context, dividerOrientation)
                addItemDecoration(divider)
            }

            if(dividerResource != 0) {
                val divider = DividerItemDecoration(context, dividerOrientation)
                ContextCompat.getDrawable(context, dividerResource)?.let { divider.setDrawable(it) }

                addItemDecoration(divider)
            }
        }
    }

    fun setData(data: MutableList<Item>) {
        (adapter as GenericAdapter).data = data
        adapter.notifyDataSetChanged()
    }

    fun setOnItemClick(listener: (Item, View) -> Unit) {
        (adapter as GenericAdapter).mOnItemClickListener = listener
    }

    fun setOnItemLongClick(listener: (Item, View) -> Unit) {
        (adapter as GenericAdapter).mOnItemLongClickListener = listener
    }

    class GenericAdapter(data: MutableList<Item>, context: Context):
            RecyclerView.Adapter<GenericVH>() {
        var mOnItemClickListener: ((Item, View) -> Unit?)? = null
        var mOnItemLongClickListener: ((Item, View) -> Unit?)? = null

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenericVH {
            val inflater = LayoutInflater.from(context)
            return GenericVH(data.first().onCreateView(parent, inflater, viewType))
        }

        var data = mutableListOf<Item>()
        private var context: Context? = null

        init {
            this.data = data
            this.context = context
        }

        override fun getItemCount(): Int {
            return data.count()
        }

        override fun getItemViewType(position: Int): Int {
            return data[position].getViewType()
        }

        override fun onBindViewHolder(holder: GenericVH, position: Int) {
            setupListeners(holder, data[position])
            holder.bindValues(data[position])
        }

        private fun setupListeners(holder: GenericVH, item: Item) {
            if(mOnItemClickListener != null) holder.itemView.setOnClickListener( {
                mOnItemClickListener?.invoke(item, it)
            })
            if(mOnItemLongClickListener != null) holder.itemView.setOnLongClickListener( {
                mOnItemLongClickListener?.invoke(item, it)
                true
            })
        }
    }

    class GenericVH(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bindValues(currentItem: Item) {
            currentItem.bindViews(itemView)
        }
    }

    abstract class Item {
        private val defaultViewType = 0
        abstract fun onCreateView(parent: ViewGroup, inflater: LayoutInflater,
                                  viewType: Int): View
        abstract fun bindViews(itemView: View)
        open fun getViewType(): Int {
            return defaultViewType
        }
    }
}