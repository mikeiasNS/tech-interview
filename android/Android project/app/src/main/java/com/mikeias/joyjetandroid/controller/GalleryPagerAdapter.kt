package com.mikeias.joyjetandroid.controller

import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mikeias.joyjetandroid.R
import com.mikeias.joyjetandroid.controller.listItems.GalleryCategoryItem
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_image.view.*

class GalleryPagerAdapter(val urls: ArrayList<String>): PagerAdapter() {
    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
        return urls.size
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as? View)
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = LayoutInflater.from(container.context)
                .inflate(R.layout.fragment_image, container, false)

        view.setOnClickListener {
            val itemView = container.parent as? ViewGroup
            itemView?.performClick()
        }

        container.addView(view)

        Picasso.get()
                .load(urls[position])
                .error(R.drawable.ic_image)
                .resize(320, 240)
                .into(view.image_view, GalleryCategoryItem.ImageLoadedCallback(view.loader, view.image_view))

        return view
    }
}