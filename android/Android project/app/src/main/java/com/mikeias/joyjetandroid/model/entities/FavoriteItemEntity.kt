package com.mikeias.joyjetandroid.model.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import com.google.gson.Gson

@Entity
class FavoriteItemEntity() {
    @PrimaryKey(autoGenerate = true)
    var id = 0
    var title = ""
    var description = ""
    var category = ""
    var gallery = arrayListOf<String>()

    @Ignore
    constructor(title: String, description: String, gallery: ArrayList<String>, category: String): this() {
        this.gallery = gallery
        this.title = title
        this.description = description
        this.category = category
    }
}