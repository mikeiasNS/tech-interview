package com.mikeias.joyjetandroid.model.daos

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.mikeias.joyjetandroid.model.entities.FavoriteItemEntity

@Dao
interface FavoriteItemDao {
    @Insert
    fun insert(item: FavoriteItemEntity)
    @Delete
    fun delete(item: FavoriteItemEntity)
    @Query("SELECT * FROM favoriteItemEntity")
    fun getAll(): List<FavoriteItemEntity>
    @Query("SELECT * FROM favoriteItemEntity WHERE title = :title AND category = :category")
    fun getByTitleAndCategory(title: String, category: String): FavoriteItemEntity?
}