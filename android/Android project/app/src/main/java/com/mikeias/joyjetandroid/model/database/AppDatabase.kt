package com.mikeias.joyjetandroid.model.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context
import com.mikeias.joyjetandroid.model.daos.FavoriteItemDao
import com.mikeias.joyjetandroid.model.entities.FavoriteItemEntity

@Database(entities = arrayOf(FavoriteItemEntity::class), version = 1)
@TypeConverters(Converters::class)
abstract class AppDatabase: RoomDatabase() {
    abstract fun favoriteItemDao(): FavoriteItemDao
    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(context.applicationContext,
                                AppDatabase::class.java, "app_database").build()
                    }
                }
            }
            return INSTANCE!!
        }
    }
}