package com.mikeias.joyjetandroid.controller.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mikeias.joyjetandroid.R
import com.mikeias.joyjetandroid.controller.SimpleRecyclerView
import com.mikeias.joyjetandroid.controller.listItems.GalleryCategoryItem
import com.mikeias.joyjetandroid.model.database.AppDatabase
import kotlinx.android.synthetic.main.fragment_list.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg

/**
 * Created by dev2 on 20/06/18.
 * for Android project at 17:23
 */
class FavoritesFragment: Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onResume() {
        super.onResume()
        activity?.title = getString(R.string.favorites)
        no_data_text.visibility = View.GONE

        setupData()

        recyclerView.setOnItemClick { item: SimpleRecyclerView.Item, _: View ->
            if(item is GalleryCategoryItem) {
                item.onClickItem(context)
            }
        }

        swipe_to_refresh.setOnRefreshListener { setupData() }
    }

    private fun setupData() {
        swipe_to_refresh.isRefreshing = true

        async(UI) {
            val favorites = bg {
                val bd = AppDatabase.getDatabase(this@FavoritesFragment.context!!)
                val databaseData = bd.favoriteItemDao().getAll()

                val favorites = arrayListOf<SimpleRecyclerView.Item>()
                for (item in databaseData) {
                    favorites.add(GalleryCategoryItem(item.title, item.description, item.gallery, item.category))
                }
                favorites
            }

            val data = favorites.await()
            recyclerView.setData(data)
            if (data.isEmpty()) {
                no_data_text.visibility = View.VISIBLE
            }
            swipe_to_refresh.isRefreshing = false
        }
    }
}