package com.mikeias.joyjetandroid.controller.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.mikeias.joyjetandroid.R
import com.mikeias.joyjetandroid.controller.listItems.GalleryCategoryItem
import com.mikeias.joyjetandroid.controller.GalleryPagerAdapter
import kotlinx.android.synthetic.main.activity_details.*
import kotlinx.android.synthetic.main.content_details.*
import android.support.v4.graphics.drawable.DrawableCompat
import android.support.annotation.ColorRes
import android.support.v4.content.ContextCompat
import com.mikeias.joyjetandroid.model.database.AppDatabase
import com.mikeias.joyjetandroid.model.entities.FavoriteItemEntity
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg

class DetailsActivity : AppCompatActivity() {
    companion object {
        const val ITEM_EXTRA = "item"
    }
    private var mItem: GalleryCategoryItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        toolbar.setNavigationIcon(R.drawable.ic_back)
        setSupportActionBar(toolbar)

        title = ""
        mItem = intent.getSerializableExtra(ITEM_EXTRA) as? GalleryCategoryItem

        setInfo()
    }

    private fun setInfo() {
        if (mItem != null ) {
            item_title.text = mItem!!.title
            description.text = mItem!!.description
            category.text = mItem!!.category

            toolbar_gallery.adapter = GalleryPagerAdapter(mItem!!.gallery!!)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_item_detail, menu)
        if(menu != null) {
            colorFavoriteButton(menu.findItem(R.id.action_favorite))
        }
        return super.onCreateOptionsMenu(menu)
    }

    private fun favoritateItem(menuItem: MenuItem) {
        async(UI) {
            val color = bg {
                var color = android.R.color.white
                if (mItem != null) {
                    val db = AppDatabase.getDatabase(this@DetailsActivity)

                    if(isItemFavorite()) {
                        val favoriteItem = db.favoriteItemDao()
                                .getByTitleAndCategory(mItem!!.title!!, mItem!!.category)
                        if(favoriteItem != null) {
                            db.favoriteItemDao().delete(favoriteItem)
                        }
                    } else {
                        val favoriteItem = FavoriteItemEntity(mItem!!.title!!, mItem!!.description!!,
                                mItem!!.gallery!!, mItem!!.category)
                        db.favoriteItemDao().insert(favoriteItem)

                        color = R.color.colorAccent
                    }
                }
                color
            }

            tintMenuIcon(menuItem, color.await())
        }
    }

    private fun colorFavoriteButton(menuItem: MenuItem) {
        async(UI) {
            val color = bg {
                var color = android.R.color.white
                if(isItemFavorite()) {
                    color = R.color.colorAccent
                }
                color
            }

            tintMenuIcon(menuItem, color.await())
        }
    }

    private fun isItemFavorite(): Boolean {
        var result = false
        if (mItem != null) {
            val db = AppDatabase.getDatabase(this@DetailsActivity)
            val dao = db.favoriteItemDao()

            if(dao.getByTitleAndCategory(mItem!!.title!!, mItem!!.category) != null) {
                result = true
            }
        }

        return result
    }

    private fun tintMenuIcon(item: MenuItem, @ColorRes color: Int) {
        val normalDrawable = item.icon
        val wrapDrawable = DrawableCompat.wrap(normalDrawable)
        DrawableCompat.setTint(wrapDrawable, ContextCompat.getColor(this, color))

        item.icon = wrapDrawable
    }

    override fun onOptionsItemSelected(menuItem: MenuItem?): Boolean {
        when(menuItem?.itemId) {
            android.R.id.home -> finish()
            R.id.action_favorite -> {
                favoritateItem(menuItem)
            }
        }
        return super.onOptionsItemSelected(menuItem)
    }
}
